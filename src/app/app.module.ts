import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from "@angular/http"

import { AppComponent } from './app.component';
import { NavigationBarComponent } from "./navigation-bar/navigation-bar.component"
import { DocentComponent } from './docent/docent.component';
import { StagesComponent } from './stages/stages.component';
import { BedrijvenComponent } from './bedrijven/bedrijven.component';
import { StudentenComponent } from './studenten/studenten.component';
import { AppRoutingModule} from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { UserService } from "./user.service";
import { AuthGuard } from "./auth.guard";
import { StudentOverzichtComponent } from "./studentOverzicht/studentOverzicht.component";
import { StageVoorstelComponent } from './stage-voorstel/stage-voorstel.component';
import { StageVoorstellenComponent } from './stage-voorstellen/stage-voorstellen.component';
import { HttpClientModule } from "@angular/common/http";
import { EindbeoordelingComponent } from './eindbeoordeling/eindbeoordeling.component';
import { StudentbegeleidingComponent } from './studentbegeleiding/studentbegeleiding.component';

@NgModule({
  declarations: [
    AppComponent,
    DocentComponent,
    StagesComponent,
    BedrijvenComponent,
    StudentenComponent,
    DashboardComponent,
    NavigationBarComponent,
    LoginComponent,
    StudentOverzichtComponent,
    StageVoorstelComponent,
    StageVoorstellenComponent,
    EindbeoordelingComponent,
    StudentbegeleidingComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule


  ],
  providers: [UserService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

